package control;

import java.awt.Font;
import java.sql.Timestamp;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class variable {
	
	/* Main Elements */
	public static final boolean DEBUG = false;
	public static final int WIDTH = 480;
	public static final int HEIGHT = 500;
	public static final String TITLE = "Quizinator";
	public static Font defaultFont;
	public static worker worker;
	
	public static JFrame currFrame;
	public static JPanel currPanel;
	public static JPanel panel_01;
	public static JPanel panel_02;
	public static JPanel panel_03;
	public static JPanel panel_05;
	public static JPanel panel_06;
	
	/* UI Elements */
	
	/* Element Sizing */
	public static int borderX = 3;
	public static int borderY = 5;
	public static int width = variable.WIDTH - 9;
	public static int height = variable.HEIGHT - 30;
	public static int elementWidth  = 250;
	public static int elementHeight = 35;
	public static int elementX = (int)(elementWidth * 0.5f) - 12;
	public static int elementY = 30;
	public static int spread = 55;
	
	/* Element Fields */
	// login
	public static String txtSubTitleLogin = "Let's get LEARNING";
	public static String txtLabelUserLogin = "Username";
	public static String txtLabelPassLogin = "Password";
	public static String txtBtnSignIn = "Log In";
	public static String txtBtnSignUp = "Register";
	public static int idUserLog = 0;
	// TODO: check these before shipping
	public static String txtUserLog = ""; // "tempME@mynew.email";
	public static String txtPassLog = ""; // "great875";
	
	// register
	public static String txtSubTitleRegister = "Sign Up for LEARNING";
	public static String txtRegEmail = "Email Address";
	public static String txtRegUser = "Username";
	public static String txtRegPass = "Password";
	public static String txtRegPass2 = "Password Again";
	public static String txtSignUp = "Sign Up";
	public static String txtRegTtl = "Thank You";
	public static String txtRegMsg = "Your are signed up and can use '" +  TITLE + "' please sign in to start the adventure.";
	
	// subject
	public static String txtSubTitleSubject = "Pick a Subject";
	public static String txtBtnNext = "Start Quiz";
	public static String txtBtnResult = "Past Results";
	public static String[] txtSubject = {};
	public static String txtSelectedSubject;
	public static Integer numSubjectIndex = 0;
	public static Integer listQNum[] = { 5, 10, 15, 20 };
	public static Integer numQuestions = 20;
	public static String txtLblQlen = "Number of Questions";
	
	// quiz
	public static String txtQuizTitleSubject = "Quiz";
	public static String txtBtnCheck = "Check Answer";
	public static String txtBtnNextQ = "Next Question";
	public static String txtBtnExit = "Stop the Quiz";
	public static String[][] questions = {};
	public static Timestamp timeStart;
	public static Timestamp timeEnd;
	public static Integer numCorrect;
	public static Integer numCurrQuest = 1;
	
	// results
	public static String txtResTitleSubject = "Past Results";
	public static String txtBtnResBack = "More Quizing";
	
	// temp
	public static String txtEmailTEMP = ""; // "tempME@mynew.email";
	public static String txtUserTEMP  = ""; // "tempMe6";
	public static String txtPassTEMP  = ""; // "great875";
	public static String txtPass2TEMP = ""; // "great875";
	
	// error
	public static String txtMsgErrorLogin = "incorrect usename, password, or connection problem";
	public static String txtErrorEmail = "Email address is invalid or incorrect.";
	public static String txtErrorUsername = "Username, at least 6, A-Z, a-z, 0-9, and '_-'.";
	public static String txtErrorPassword = "Paswword, at least 8, letter number symbol.";
	public static String txtErrorPassMatch = "Passwords do not match.";
	public static String txtErrorConnection = "Connection failure.";
	public static String txtErrorProblem = "Please fix the problem(s) and try again.";
} // variable
