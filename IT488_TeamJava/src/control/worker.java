package control;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import panels.panel_login;
import panels.panel_quiz;
import panels.panel_register;
import panels.panel_result;
import panels.panel_subject;

public class worker {

	/* connection fields */
	private Connection conn = null;
	private String dbhost;
	private Properties dbinfo;
	private Object userinfo[];
	private String systemipaddress = "";
	
	public Integer numSubjectIndex;
	public Integer numQuestions;
	public Integer[] answers;
	public String[][] questions;
	public String[] txtSubject = {};
	public String txtSelectedSubject;
	
	/* constructor */
	public worker() {
		dbhost = "jdbc:mysql://www.laststrawlabs.com:3306/laststra_data";
		dbinfo = new Properties();
		dbinfo.put("user", "laststra_suroot");
		dbinfo.put("password", "MOwrj8m1O@!U");
		dbinfo.put("encrypt", true);
		dbinfo.put("trustServerCertificate", false);
		dbinfo.put("loginTimeout", 2);
	} // constructor
	
	
	/* panel switcher */
	public void panelSwitch(String panelName) {
		variable.currPanel.removeAll();
		variable.currFrame.remove(variable.currPanel);
		switch(panelName) {
			case "register": {
				variable.currPanel = new panel_register();
			} break;
			case "subject": {
				variable.txtSubject = getSubjectData();
				variable.currPanel = new panel_subject();
			} break;
			case "quiz": {
				variable.currPanel = new panel_quiz();
			} break;
			case "result": {
				variable.currPanel = new panel_result();
			} break;
			case "login":
			default: {
				variable.currPanel = new panel_login();
			} break;
		}
		variable.currFrame.revalidate();
		variable.currFrame.repaint();
	} // panel switcher

	/* Panel login */
	public boolean validateLogin(String email, String pass) {
		if(!isConnected()) return false;
		if(isUnique("email", email)) {
			if(isUnique("username", email)) {
				return false;
			}
		}
		userinfo = getUserInfo(email, pass);
		if((boolean)userinfo[2]) {
			logLoginTime((String)userinfo[0]); // by username
			return true;
		}
		userinfo = null;
		return false;
	} // validateLogin
	
	/* valid email format */
	private boolean isValidEmail(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	} // isValidEmail
	
	/* valid username format */
	private boolean isValidUser(String user) {
		/* username size at least 6 */
		String regex = "^(?=[a-zA-Z0-9._]{6,20}$)(?!.*[_.]{2})[^_.].*[^_.]$";
		if(!user.matches(regex))  return false;
		/* check the database */
		return isUnique("username", user);
	} // isValidUser
	
	
	/* valid password format */
	private boolean validatePass(String pass) {
		/* password is empty */
        if (pass == null) return false;
        /* Regex to check valid password */ 
        String regex = "^(?=.*[A-Za-z])(?=.*[0-9])(?=\\S+$).{8,32}$";
		Pattern p = Pattern.compile(regex); 
        Matcher m = p.matcher(pass);
        return m.matches();		
	} // validatePass
	
	/* validate sign up */
	@SuppressWarnings("unused")
	public List<String> validateSignup(String email, String user, String pass, String pass2, boolean test) {
		List<String> error = new ArrayList<String>(); // No Error
		int count = 0;
		if(!isConnected()) {
			error.add(variable.txtErrorConnection);
			return error;
		}
		if(!isValidEmail(email)) error.add(variable.txtErrorEmail);
		if(!isValidUser(user))   error.add(variable.txtErrorUsername);
		if(!validatePass(pass))  error.add(variable.txtErrorPassword);
		if(!pass.equals(pass2))  error.add(variable.txtErrorPassMatch);
		if(count == 0 && !variable.DEBUG) {
			boolean newUser = addNewUser(email, user, pass);
			if(!newUser) {
				error.add(variable.txtErrorProblem);
			} else {
				variable.txtUserLog = user;
				variable.txtPassLog = pass;
			}
		}
		return error;
	} // validateSignup
	
	
	/*************************************************************************
	 *** DATABASE
	 *************************************************************************/
	
	/* isConnected */
	public boolean isConnected() {
		boolean result = false;
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) result = true;
			if(!conn.isClosed()) conn.close();
		} catch (CommunicationsException e) {
//			e.printStackTrace();
			System.out.println("Communications link failure");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception");
		}
		return result;
	} // isConnected
	
	/* isUnique */
	private boolean isUnique(String column, String object) {
		boolean found = false;
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				String query = 
					"SELECT COUNT(*) FROM `users` WHERE `" + column + "` = '" + object + "'";
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);
				rs.next();
				found = rs.getString("COUNT(*)").equals("0");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return found;
	} // isUnique

	/* getUserInfo */
	private Object[] getUserInfo(String usercred, String passcred) {
		Object result[] = { null, null, false };
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				String query = 
					"SELECT `email`, `username` FROM `users` WHERE (`email` = '" + usercred + 
					"' OR `username` = '" + usercred + "') AND `password` = '" + passcred + "' LIMIT 1";
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);
				if(rs.next()) {
					result[0] = rs.getString("username");
					result[1] = rs.getString("email");
					result[2] = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	} // getUserInfo
	
	/* getSystemIP */
	private String getSystemIP() {
		String ipaddress = "";
		try {
		URL url_name = new URL("http://bot.whatismyipaddress.com");
		BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream()));
		ipaddress = sc.readLine().trim();
		} catch (Exception e) {
			ipaddress = "";
		}
		return ipaddress;
	} // getSystemIP
	
	/* logLoginTime */
	private boolean logLoginTime(String username) {
		if(systemipaddress.isEmpty()) systemipaddress = getSystemIP();
		int rs = 0;
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				String query =
					"UPDATE `users` SET `login` = NOW(), `ip` = '" + systemipaddress + 
					"' WHERE `username` = '" + username + "'";
				Statement stmt = conn.createStatement();
				rs = stmt.executeUpdate(query);
				if(rs != 0) {
					query = "SELECT `ID` FROM `users` WHERE `username` = '" + username + "'";
					ResultSet rst = stmt.executeQuery(query);
					rst.next();
					variable.idUserLog = rst.getInt("ID");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (rs == 0) ? false : true;
	} // logLoginTime
	
	/* addNewUser */
	private boolean addNewUser(String email, String user, String pass) {
		if(systemipaddress.isEmpty()) systemipaddress = getSystemIP();
		int rs = 0;
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				String query =
					"INSERT INTO `users` (`email`, `username`, `password`, `signup`, `login`, `ip`) VALUES " + 
					"('" + email + "', '" + user + "', '" + pass + "', NOW(), NULL, '" + systemipaddress + "')";
				Statement stmt = conn.createStatement();
				rs = stmt.executeUpdate(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return (rs == 0) ? false : true;
	} // addNewUser
	
	/* getSubjectData */
	public String[] getSubjectData() {
		if(variable.txtSubject.length != 0) return variable.txtSubject;
		ArrayList<String> result = new ArrayList<String>();
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				String query = "SELECT `subject` FROM `subjects` WHERE 1";
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);
				while(rs.next()) {
					result.add(rs.getString(1));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		String[] stringArray = result.toArray(new String[0]);
		return stringArray;
	} // getSubjectData
	
	/* getQuestions */
	public String[][] getQuestions(Integer subject, Integer quest) {
		String[][] questions = new String[quest][7];
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				String query = 
					"SELECT `ID`,`question`,`answer1`,`answer2`,`answer3`,`answer4`,`correct` FROM `questions` WHERE `subject` = " + 
					subject + " ORDER BY RAND() LIMIT " + quest;
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);
				int i = 0;
				while(rs.next()) {
					questions[i][0] = rs.getString("question");
					questions[i][1] = rs.getString("answer1");
					questions[i][2] = rs.getString("answer2");
					questions[i][3] = rs.getString("answer3");
					questions[i][4] = rs.getString("answer4");
					questions[i][5] = rs.getString("correct");
					questions[i][6] = rs.getString("ID");
					++i;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return questions;
	} /* getQuestions */
	
	/* sendResult */
	public void sendResult(int user, int subject, int quest, int right, Timestamp starttime, Timestamp endtime, String quiz) {
		String query = "INSERT INTO `results` (`user`, `subject`, `quest`, `correct`, `starttime`, `endtime`, `quiz`) VALUES " +
			"(" + user + ", " + subject + ", " + quest + ", " + right + ", \"" + starttime + "\", \"" + endtime + "\", \'" + quiz + "\')";
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	} // sendResult
	
	/* getResultsData */
	public Object[][] getResultsData() {
		Object[][] resultsData = {
			{ "", "", "", "", "", "", "" },
			{ "", "", "", "", "", "", "" }
		};
		try {
			conn = DriverManager.getConnection(dbhost, dbinfo);
			if(conn != null) {
				String query = "SELECT count(*) FROM `results` WHERE `user`=" + variable.idUserLog;
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);
				rs.next();
				int rows = rs.getInt("count(*)");
				int cols = 7;
				resultsData = new Object[rows][cols];
				query = "SELECT * FROM `results` WHERE `user`=" + variable.idUserLog + " ORDER BY `starttime` DESC ";
				stmt = conn.createStatement();
				rs = stmt.executeQuery(query);
				int i = 0;
				while(rs.next()) {
					int quest = rs.getInt("quest");
					int correct = rs.getInt("correct");
					float percent = ((float)correct / quest) * 100.0f;
					Timestamp starttime = rs.getTimestamp("starttime");
					Timestamp endtime = rs.getTimestamp("endtime");
					Timestamp time = new Timestamp(endtime.getTime() - starttime.getTime());
					DateFormat shrtf = new SimpleDateFormat("mm:ss");
					DateFormat longf = new SimpleDateFormat("HH:mm:ss MM-dd-yyyy");
					resultsData[i][0] = variable.txtSubject[rs.getInt("subject") - 1];
					resultsData[i][1] = quest;
					resultsData[i][2] = correct;
					resultsData[i][3] = (int)percent + "%";
					resultsData[i][4] = shrtf.format(time);
					resultsData[i][5] = longf.format(starttime);
					resultsData[i][6] = longf.format(endtime);
					i++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resultsData;
	} // getResultsData
	
	
} // worker
