package main;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import control.variable;
import control.worker;
import panels.panel_login;

public class start {

	public static void main(String[] args) {
		
		JFrame window = new JFrame("IT488 " + variable.TITLE);
		
		/* get default font */
		Graphics g = new BufferedImage(variable.WIDTH, variable.HEIGHT, BufferedImage.TYPE_INT_RGB).getGraphics();
		variable.defaultFont = new Font(g.getFont().toString(), 0, 12);
        g.dispose();
		
		/* create the panels */
        variable.worker = new worker();
        variable.txtSubject = variable.worker.getSubjectData();
		variable.currFrame = window;
        variable.panel_01 = new panel_login();
		
		window.setPreferredSize(new Dimension(variable.WIDTH + 3, variable.HEIGHT + 9));
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		window.pack();
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		
	} // main

} // EOF
