package panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import control.variable;

@SuppressWarnings("serial")
public class panel_login extends JPanel {
	
	/* constructor */
	public panel_login() {
		variable.currPanel = this;
		setPreferredSize(new Dimension(variable.WIDTH, variable.HEIGHT));
		setLayout(null);
		setFocusable(true);
		requestFocus();
		createGUI();
	} // constructor
	
	
	/* createGUI */
	private void createGUI() {
		
		/* adjustable sizing */
		int borderX = variable.borderX;
		int borderY = variable.borderY;
		int width = variable.width;
		int height = variable.height;
		int elementWidth = variable.elementWidth;
		int elementHeight = variable.elementHeight;
		int elementX = variable.elementX;
		int elementY = variable.elementY;
		int spread = variable.spread;
		
		/* title label */
		JLabel lblTitle = new JLabel(variable.TITLE);
		lblTitle.setFont(new Font(variable.defaultFont.toString(), Font.BOLD, (int)(spread * 0.70f)));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblTitle);
		
		/* subtitle label */
		elementY += (int)(spread * 0.50f);
		JLabel lblSubTitle = new JLabel(variable.txtSubTitleLogin);
		lblSubTitle.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblSubTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblSubTitle);
		
		/* username text field */
		elementY += spread;
		JLabel lblUser = new JLabel(variable.txtLabelUserLogin);
		lblUser.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblUser.setBounds(elementX + 5, elementY - (int)(elementHeight * 0.75f) + 2, elementWidth, elementHeight);
		variable.currPanel.add(lblUser);
		
		JTextField txfUser = new JTextField();
		txfUser.setBounds(elementX, elementY, elementWidth, elementHeight);
		txfUser.setMargin(new Insets(0, 5, 0, 0));
		txfUser.setText(variable.txtUserLog);
		variable.currPanel.add(txfUser);
		
		/* password text field */
		elementY += spread;
		JLabel lblPass = new JLabel(variable.txtLabelPassLogin);
		lblPass.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblPass.setBounds(elementX + 5, elementY - (int)(elementHeight * 0.75f) + 2, elementWidth, elementHeight);
		variable.currPanel.add(lblPass);
		
		JTextField txfPass = new JTextField();
		txfPass.setBounds(elementX, elementY, elementWidth, elementHeight);
		txfPass.setMargin(new Insets(0, 5, 0, 0));
		txfPass.setText(variable.txtPassLog);
		variable.currPanel.add(txfPass);
		
		/* ERROR label */
		elementY += spread;
		JLabel lblError = new JLabel(variable.txtMsgErrorLogin);
		lblError.setForeground(Color.RED);
		lblError.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setBounds(elementX, elementY - (int)(spread * 0.5f), elementWidth, elementHeight);
		lblError.setToolTipText(variable.txtMsgErrorLogin);
		lblError.setVisible(false);
		variable.currPanel.add(lblError);

		/* login button */
		JButton btnLogin = new JButton(variable.txtBtnSignIn);
		btnLogin.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/* validation */
				if(variable.DEBUG) {
					variable.worker.panelSwitch("subject");
				} else {
					if(variable.worker.validateLogin(txfUser.getText(), txfPass.getText()) ) { // true
						variable.worker.panelSwitch("subject");
					} else { // false
						lblError.setVisible(true);
					}
				}
			}
		});
		variable.currPanel.add(btnLogin);
		
		/* register button */
		elementY += spread;
		JButton btnRegister = new JButton(variable.txtBtnSignUp);
		btnRegister.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnRegister.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				variable.worker.panelSwitch("register");
			}
		});
		variable.currPanel.add(btnRegister);
		
		/* outline boarder */
		JPanel panelBorder = new JPanel();	
		panelBorder.setBounds(borderX, borderY, width, height);
		panelBorder.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray)));
		variable.currPanel.add(panelBorder);
		
		/* add the panel to the frame */
		variable.currFrame.add(variable.currPanel);
		
	} // createGUI
	
	
	
} // panel_login
