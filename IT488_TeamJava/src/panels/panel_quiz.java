package panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import control.variable;

@SuppressWarnings("serial")
public class panel_quiz extends JPanel {
	
	/* fields */
	JLabel lblQuestNum;
	JTextArea lblQuestion;
	ButtonGroup group;
	JRadioButton[] btnRadio = {
		new JRadioButton(),
		new JRadioButton(),
		new JRadioButton(),
		new JRadioButton()
	};
	JPanel[] radioBorder = {
		new JPanel(),
		new JPanel(),
		new JPanel(),
		new JPanel()
	};
	Boolean setCorrect = false;
	String JSONhead;
	String JSONbody;
	String JSONtail;
	
	/* constructor */
	public panel_quiz() {
		variable.currPanel = this;
		setPreferredSize(new Dimension(variable.WIDTH, variable.HEIGHT));
		setLayout(null);
		setFocusable(true);
		requestFocus();
		createGUI();
		onLoad();
	} // constructor
	
	
	/* createGUI */
	private void createGUI() {
		
		/* adjustable sizing */
		int borderX = variable.borderX;
		int borderY = variable.borderY;
		int width = variable.width;
		int height = variable.height;
		int elementWidth = variable.elementWidth;
		int elementHeight = variable.elementHeight;
		int elementX = variable.elementX;
		int elementY = variable.elementY;
		int spread = variable.spread;
		
		/* title label */
		JLabel lblTitle = new JLabel(variable.TITLE);
		lblTitle.setFont(new Font(variable.defaultFont.toString(), Font.BOLD, (int)(spread * 0.70f)));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblTitle);
		
		/* subtitle label */
		elementY += (int)(spread * 0.50f);
		JLabel lblSubTitle = new JLabel(variable.txtSelectedSubject);
		lblSubTitle.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblSubTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblSubTitle);
		
		/* question */
		elementY += spread;
		lblQuestNum = new JLabel("1. ");
		lblQuestNum.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblQuestNum.setBounds(elementX - 75, elementY - 21, elementWidth, 20);
		variable.currPanel.add(lblQuestNum);
		
		lblQuestion = new JTextArea();
		lblQuestion.setText(""); //txtQuestion[numQuizCur - 1]);
		lblQuestion.setWrapStyleWord(true);
		lblQuestion.setLineWrap(true);
		lblQuestion.setOpaque(false);
		lblQuestion.setEditable(false);
		lblQuestion.setFocusable(false);
		lblQuestion.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblQuestion.setBounds(elementX - 50, elementY - 20, elementWidth + 100, elementHeight + 20);
		lblQuestion.setBackground(UIManager.getColor("Label.background"));
		lblQuestion.setBorder(UIManager.getBorder("Label.border"));
		variable.currPanel.add(lblQuestion);
		
		/* answer */
		elementY += spread;
		
		// Group the radio buttons.
		int rSpace = 23;
		int rAdjust = -15;
		group = new ButtonGroup();
		for (int i = 0; i < 4; i++) {
			btnRadio[i].setBounds(elementX, elementY + rAdjust + (i * rSpace), elementWidth, 20);
			group.add(btnRadio[i]);
			btnRadio[i].setBorder(BorderFactory.createEmptyBorder());
			variable.currPanel.add(btnRadio[i]);
		}
		btnRadio[0].setSelected(true);
		
		/* check */
		elementY += spread + spread;
		JButton btnCheck = new JButton(variable.txtBtnCheck);
		btnCheck.setBounds(elementX, elementY, (int)(elementWidth * 0.49f), elementHeight);
		btnCheck.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onCheck();
			}
		});
		variable.currPanel.add(btnCheck);
		
		/* next */
		JButton btnNextQ = new JButton(variable.txtBtnNextQ);
		btnNextQ.setBounds(elementX + (int)(elementWidth * 0.51f), elementY, (int)(elementWidth * 0.49f), elementHeight);
		btnNextQ.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onNext();
			}
		});
		variable.currPanel.add(btnNextQ);
		
		/* quit */
		elementY += spread;
		JButton btnExit = new JButton(variable.txtBtnExit);
		btnExit.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onExit();
			}
		});
		variable.currPanel.add(btnExit);
		
		/* radio button boarder */
		for(int j = 0; j < btnRadio.length; j++) {
			radioBorder[j].setVisible(false);
			radioBorder[j].setBorder(BorderFactory.createEmptyBorder());
			btnRadio[j].add(radioBorder[j]);
		}
		
		/* outline boarder */
		JPanel panelBorder = new JPanel();	
		panelBorder.setBounds(borderX, borderY, width, height);
		panelBorder.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray)));
		variable.currPanel.add(panelBorder);
		
		/* add the panel to the frame */
		variable.currFrame.add(variable.currPanel);
		
	} // createGUI
	
	/* onLoad */
	private void onLoad() {
		variable.questions = variable.worker.getQuestions(variable.numSubjectIndex + 1, variable.numQuestions);
		variable.numCurrQuest = 1;
		lblQuestNum.setText(variable.numCurrQuest + ". ");
		lblQuestion.setText(variable.questions[variable.numCurrQuest - 1][0]);
		JSONhead = ""
			+ "{"
			+ 	"\"array\": {";
//		JSONbody = ""
//			+ 		"\"1\": [1, 2, 3],"
//			+ 		"\"2\": [1, 2, 3],"
//			+ 		"\"3\": [1, 2, 3],"
//			+ 		"\"4\": [1, 2, 3],"
//			+ 		"\"5\": [1, 2, 3],";
		JSONbody = "";
		JSONtail = ""
			+ 		"\"index\": ["
			+ 			"\"question\","
			+ 			"\"correct\","
			+ 			"\"selected\""
			+ 		"]"
			+ 	"}"
			+ "}";
		for (int j = 0; j < 4; j++) {
			btnRadio[j].setText(variable.questions[0][j + 1]);
		}
		variable.numCorrect = 0;
		variable.timeStart = new Timestamp(System.currentTimeMillis());
	} // onLoad
	
	/* onCheck */
	private void onCheck() {
		int correct = Integer.parseInt(variable.questions[variable.numCurrQuest - 1][5]) - 1;
		int selected = 1;
		for (int i = 0; i < btnRadio.length; i++) {
			if(btnRadio[i].isSelected()) selected = i + 1;
		}
		for (int i = 0; i < btnRadio.length; i++) {
			btnRadio[i].setEnabled(false);
			if(i == (selected - 1)) {
				radioBorder[selected - 1].setVisible(true);
				radioBorder[selected - 1].setOpaque(false);
				radioBorder[selected - 1].setBorder(BorderFactory.createLineBorder(Color.RED));
			}
			if(i == correct) {
				radioBorder[correct].setVisible(true);
				radioBorder[correct].setOpaque(false);
				radioBorder[correct].setBorder(BorderFactory.createLineBorder(Color.GREEN));
			}
			if((selected - 1) == correct) setCorrect = true;
		}
	} // onCheck
	
	/* onNext */
	private void onNext() {
		if(variable.numCurrQuest != variable.numQuestions) {
			for (int i = 0; i < btnRadio.length; i++) {
				radioBorder[i].setVisible(false);
			}
		}
		if(variable.numCurrQuest < variable.numQuestions) {
			int question = Integer.parseInt(variable.questions[variable.numCurrQuest - 1][6]);
			int correct = Integer.parseInt(variable.questions[variable.numCurrQuest - 1][5]);
			int selected = 1;
			for (int i = 0; i < btnRadio.length; i++) {
				if(btnRadio[i].isSelected()) selected = i + 1;
			}
			String entry = "\"" + variable.numCurrQuest + "\": [" + question + ", " + correct + ", " + selected + "],";
			JSONbody += entry;
			if(correct == selected) variable.numCorrect++;
			for (int i = 0; i < btnRadio.length; i++) {
				btnRadio[i].setEnabled(true);
			}
			lblQuestNum.setText((variable.numCurrQuest + 1) + ". ");
			if(variable.numCurrQuest != variable.numQuestions) {
				lblQuestion.setText(variable.questions[variable.numCurrQuest][0]);
				for (int j = 0; j < 4; j++) {
					btnRadio[j].setText(variable.questions[variable.numCurrQuest][j + 1]);
				}
				btnRadio[0].setSelected(true);
			}
		}
		variable.numCurrQuest++;
		if(variable.numCurrQuest > variable.numQuestions)
			variable.numCurrQuest = variable.numQuestions;
	} // onNext
	
	/* onExit */
	private void onExit() {
		variable.timeEnd = new Timestamp(System.currentTimeMillis());
		int question = Integer.parseInt(variable.questions[variable.numCurrQuest - 1][6]);
		int correct = Integer.parseInt(variable.questions[variable.numCurrQuest - 1][5]);
		int selected = 1;
		for (int i = 0; i < btnRadio.length; i++) {
			if(btnRadio[i].isSelected()) selected = i + 1;
		}
		if(correct == selected) variable.numCorrect++;
		String entry = "\"" + variable.numCurrQuest + "\": [" + question + ", " + correct + ", " + selected + "],";
		JSONbody += entry;
		int user = variable.idUserLog;
		int subject = (variable.numSubjectIndex + 1);
		int quest = variable.numQuestions;
		int right = variable.numCorrect;
		Timestamp starttime = variable.timeStart;
		Timestamp endtime = variable.timeEnd;
		String quiz = JSONhead + JSONbody + JSONtail;
		// record results in database
		variable.worker.sendResult(user, subject, quest, right, starttime, endtime, quiz);
		variable.worker.panelSwitch("result");
	} // onExit
	
	
} // panel_quiz
