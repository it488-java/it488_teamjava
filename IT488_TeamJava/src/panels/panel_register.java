package panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import control.variable;

@SuppressWarnings("serial")
public class panel_register extends JPanel {

	/* constructor */
	public panel_register() {
		variable.currPanel = this;
		setPreferredSize(new Dimension(variable.WIDTH, variable.HEIGHT));
		setLayout(null);
		setFocusable(true);
		requestFocus();
		createGUI();
	} // constructor
	
	
	/* createGUI */
	private void createGUI() {
		
		/* adjustable sizing */
		int borderX = variable.borderX;
		int borderY = variable.borderY;
		int width = variable.width;
		int height = variable.height;
		int elementWidth = variable.elementWidth;
		int elementHeight = variable.elementHeight;
		int elementX = variable.elementX;
		int elementY = variable.elementY;
		int spread = variable.spread;
		
		/* title label */
		JLabel lblTitle = new JLabel(variable.TITLE);
		lblTitle.setFont(new Font(variable.defaultFont.toString(), Font.BOLD, (int)(spread * 0.70f)));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblTitle);
		
		/* subtitle label */
		elementY += (int)(spread * 0.50f);
		JLabel lblSubTitle = new JLabel(variable.txtSubTitleRegister);
		lblSubTitle.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblSubTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblSubTitle);
		
		/* email text field */
		elementY += spread;
		JLabel lblEmail = new JLabel(variable.txtRegEmail); 
		lblEmail.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblEmail.setBounds(elementX + 5, elementY - (int)(elementHeight * 0.75f) + 2, elementWidth, elementHeight);
		variable.currPanel.add(lblEmail);
		
		JTextField txfEmail = new JTextField();
		if(variable.txtEmailTEMP != null && !variable.txtEmailTEMP.isEmpty()) txfEmail.setText(variable.txtEmailTEMP);
		txfEmail.setBounds(elementX, elementY, elementWidth, elementHeight);
		txfEmail.setMargin(new Insets(0, 5, 0, 0));
		variable.currPanel.add(txfEmail);
		
		/* username text field */
		elementY += spread;
		JLabel lblUser = new JLabel(variable.txtRegUser); 
		lblUser.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblUser.setBounds(elementX + 5, elementY - (int)(elementHeight * 0.75f) + 2, elementWidth, elementHeight);
		variable.currPanel.add(lblUser);
		
		JTextField txfUser = new JTextField();
		if(variable.txtUserTEMP != null && !variable.txtUserTEMP.isEmpty()) txfUser.setText(variable.txtUserTEMP);
		txfUser.setBounds(elementX, elementY, elementWidth, elementHeight);
		txfUser.setMargin(new Insets(0, 5, 0, 0));
		variable.currPanel.add(txfUser);
		
		/* password text field */
		elementY += spread;
		JLabel lblPass = new JLabel(variable.txtRegPass);
		lblPass.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblPass.setBounds(elementX + 5, elementY - (int)(elementHeight * 0.75f) + 2, elementWidth, elementHeight);
		variable.currPanel.add(lblPass);
		
		JTextField txfPass = new JTextField();
		if(variable.txtPassTEMP != null && !variable.txtPassTEMP.isEmpty()) txfPass.setText(variable.txtPassTEMP);
		txfPass.setBounds(elementX, elementY, elementWidth, elementHeight);
		txfPass.setMargin(new Insets(0, 5, 0, 0));
		variable.currPanel.add(txfPass);
		
		/* password text field */
		elementY += spread;
		JLabel lblPass2 = new JLabel(variable.txtRegPass2);
		lblPass2.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblPass2.setBounds(elementX + 5, elementY - (int)(elementHeight * 0.75f) + 2, elementWidth, elementHeight);
		variable.currPanel.add(lblPass2);
		
		JTextField txfPass2 = new JTextField();
		if(variable.txtPass2TEMP != null && !variable.txtPass2TEMP.isEmpty()) txfPass2.setText(variable.txtPass2TEMP);
		txfPass2.setBounds(elementX, elementY, elementWidth, elementHeight);
		txfPass2.setMargin(new Insets(0, 5, 0, 0));
		variable.currPanel.add(txfPass2);
		
		/* ERROR label */
		elementY += spread;
		JLabel lblError = new JLabel();
		lblError.setForeground(Color.RED);
		lblError.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblError.setHorizontalAlignment(SwingConstants.CENTER);
		lblError.setVerticalAlignment(SwingConstants.TOP);
		lblError.setBounds(elementX, elementY - (int)(spread * 0.40f), elementWidth + 10, elementHeight);
		variable.currPanel.add(lblError);
		
		/* sign up button */
		JButton btnRegister = new JButton(variable.txtSignUp);
		elementY += spread;
		btnRegister.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnRegister.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				/* validation */
				lblError.setText("");
				String errorText = "<html>";
				List<String> error = variable.worker.validateSignup(
					txfEmail.getText(), txfUser.getText(), txfPass.getText(), txfPass2.getText(), false
				);
				if(error.size() > 0) {
					for (String string : error) {
						errorText += string;
						errorText += "<br>";
					}
					errorText += "</html>";
					lblError.setText(errorText);
					lblError.setToolTipText(errorText);
				} else {
					/* No Error */
			    	JOptionPane.showMessageDialog(null, variable.txtRegMsg, variable.txtRegTtl, JOptionPane.INFORMATION_MESSAGE);
					variable.worker.panelSwitch("login");
				}
			}
		});
		variable.currPanel.add(btnRegister);
		
		/* sign up button */
		JButton btnCancel = new JButton("Cancel");
		elementY -= spread - 15;
		btnCancel.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lblError.setText("");
				variable.worker.panelSwitch("login");
			}
		});
		variable.currPanel.add(btnCancel);
		
		/* outline boarder */
		JPanel panelBorder = new JPanel();	
		panelBorder.setBounds(borderX, borderY, width, height);
		panelBorder.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray)));
		variable.currPanel.add(panelBorder);
		
		/* add the panel to the frame */
		variable.currFrame.add(variable.currPanel);
		
	} // createGUI
	
	
	
} // panel_register
