package panels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import control.variable;

@SuppressWarnings("serial")
public class panel_result extends JPanel {
	
	/* constructor */
	public panel_result() {
		variable.currPanel = this;
		setPreferredSize(new Dimension(variable.WIDTH, variable.HEIGHT));
		setLayout(null);
		setFocusable(true);
		requestFocus();
		createGUI();
	} // constructor
	
	
	/* createGUI */
	private void createGUI() {
		
		/* adjustable sizing */
		int borderX = variable.borderX;
		int borderY = variable.borderY;
		int width = variable.width;
		int height = variable.height;
		int elementWidth = variable.elementWidth;
		int elementHeight = variable.elementHeight;
		int elementX = variable.elementX;
		int elementY = variable.elementY;
		int spread = variable.spread;
		
		/* title label */
		JLabel lblTitle = new JLabel(variable.TITLE);
		lblTitle.setFont(new Font(variable.defaultFont.toString(), Font.BOLD, (int)(spread * 0.70f)));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblTitle);
		
		/* subtitle label */
		elementY += (int)(spread * 0.50f);
		JLabel lblSubTitle = new JLabel(variable.txtResTitleSubject);
		lblSubTitle.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblSubTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblSubTitle);
		
		/* adjust column size */
		elementWidth = 452;
		elementX = 13;
		
		String[] columnNames = { "Subject", "#", "A+", "%", "Time", "Start", "End" };
		int colSize[] = {110, 32, 32, 40, 54, 83, 83};
		Object[][] data = variable.worker.getResultsData();
		
		/* table */
		JTable table = new JTable(data, columnNames);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setIntercellSpacing(new Dimension(5, 0));
		table.setRowHeight(20);
		table.setAutoCreateRowSorter(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		/* table header */
		JTableHeader header = table.getTableHeader();
	    header.setBackground(Color.WHITE);
	    
		/* table columns */
		TableColumn column = null;
		for (int i = 0; i < colSize.length; i++) {
			column = table.getColumnModel().getColumn(i);
			column.setMinWidth(colSize[i]);
			column.setPreferredWidth(colSize[i]);
		}
		
		/* scrollPane for the table */
		elementY += spread;
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(elementX, elementY - 20, elementWidth, (int)(spread * 4.5f) + 40);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		add(scrollPane);
		
		/* adjust column size */
		elementWidth = 250;
		elementX = (int)(elementWidth * 0.5f) - 12;
		
		/* spacer */
		elementY += spread;
		elementY += spread;
		elementY += spread;
		elementY += spread;
		
		/* next button */
		elementY += spread;
		JButton btnBack = new JButton(variable.txtBtnResBack);
		btnBack.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnBack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				variable.worker.panelSwitch("subject");
			}
		});
		variable.currPanel.add(btnBack);
		
		/* outline boarder */
		JPanel panelBorder = new JPanel();	
		panelBorder.setBounds(borderX, borderY, width, height);
		panelBorder.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray)));
		variable.currPanel.add(panelBorder);
		
		/* add the panel to the frame */
		variable.currFrame.add(variable.currPanel);
		
	} // createGUI
	
	
} // panel_result
