package panels;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import control.variable;

@SuppressWarnings("serial")
public class panel_subject extends JPanel {
	
	/* constructor */
	public panel_subject() {
		variable.currPanel = this;
		setPreferredSize(new Dimension(variable.WIDTH, variable.HEIGHT));
		setLayout(null);
		setFocusable(true);
		requestFocus();
		createGUI();
	} // constructor
	
	
	/* createGUI */
	private void createGUI() {
		
		/* adjustable sizing */
		int borderX = variable.borderX;
		int borderY = variable.borderY;
		int width = variable.width;
		int height = variable.height;
		int elementWidth = variable.elementWidth;
		int elementHeight = variable.elementHeight;
		int elementX = variable.elementX;
		int elementY = variable.elementY;
		int spread = variable.spread;
		
		/* title label */
		JLabel lblTitle = new JLabel(variable.TITLE);
		lblTitle.setFont(new Font(variable.defaultFont.toString(), Font.BOLD, (int)(spread * 0.70f)));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblTitle);
		
		/* subtitle label */
		elementY += (int)(spread * 0.50f);
		JLabel lblSubTitle = new JLabel(variable.txtSubTitleSubject);
		lblSubTitle.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblSubTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubTitle.setBounds(elementX, elementY, elementWidth, elementHeight);
		variable.currPanel.add(lblSubTitle);
		
		/* subject area */
		elementY += spread;
		JList<String> lstSubjects = new JList<String>(variable.txtSubject);
		lstSubjects.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstSubjects.setLayoutOrientation(JList.VERTICAL);
		lstSubjects.setBorder(new EmptyBorder(0, 5, 0, 5));
		lstSubjects.setVisibleRowCount(-1);
		lstSubjects.setSelectedIndex(0);
		setSelectedIndex(0);
		lstSubjects.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				setSelectedIndex(lstSubjects.getSelectedIndex());
			}
		});
		
		JScrollPane scrollPane = new JScrollPane(lstSubjects);
		scrollPane.setBounds(elementX, elementY, elementWidth, (int)(spread * 2.5f));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		variable.currPanel.add(scrollPane);
		
		/* title label */
		elementY += spread * 3;
		JLabel lblQlen = new JLabel(variable.txtLblQlen);
		lblQlen.setFont(new Font(variable.defaultFont.toString(), Font.PLAIN, (int)(spread * 0.25f)));
		lblQlen.setHorizontalAlignment(SwingConstants.LEFT);
		lblQlen.setBounds(elementX, elementY - elementHeight + (int)(elementHeight * 0.25f), elementWidth, elementHeight);
		variable.currPanel.add(lblQlen);
		
		/* number of questions */
        String listQNumStr[] = new String[variable.listQNum.length];
        for (int i = 0; i < variable.listQNum.length; i++)
        	listQNumStr[i] = String.valueOf(variable.listQNum[i]);
        JComboBox<String> boxType = new JComboBox<String>(listQNumStr);
		boxType.setBackground(Color.WHITE);
		boxType.setBounds(elementX, elementY, elementWidth, elementHeight);
		boxType.setRenderer( new DefaultListCellRenderer() {
		    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		        JComponent comp = (JComponent) super.getListCellRendererComponent(list,value, index, isSelected, cellHasFocus);
		        comp.setBorder(new EmptyBorder(5, 5, 0, 0));
		        return comp;
		    }
		});
		variable.numQuestions = variable.listQNum[0];
		boxType.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				variable.numQuestions = variable.listQNum[boxType.getSelectedIndex()];
			}
		});
		variable.currPanel.add(boxType);
		
		/* next button */
		elementY += spread;
		JButton btnNext = new JButton(variable.txtBtnNext);
		btnNext.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnNext.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				variable.worker.panelSwitch("quiz");
			}
		});
		variable.currPanel.add(btnNext);
		
		/* results button */
		elementY += spread;
		JButton btnResults = new JButton(variable.txtBtnResult);
		btnResults.setBounds(elementX, elementY, elementWidth, elementHeight);
		btnResults.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				variable.worker.panelSwitch("result");
			}
		});
		variable.currPanel.add(btnResults);
		
		/* outline boarder */
		JPanel panelBorder = new JPanel();	
		panelBorder.setBounds(borderX, borderY, width, height);
		panelBorder.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.gray)));
		variable.currPanel.add(panelBorder);
		
		/* add the panel to the frame */
		variable.currFrame.add(variable.currPanel);
		
	} // createGUI
	
	private void setSelectedIndex(int num) {
		variable.numSubjectIndex = num;
		variable.txtSelectedSubject = variable.txtSubject[variable.numSubjectIndex];
	}	
	
} // panel_subject
